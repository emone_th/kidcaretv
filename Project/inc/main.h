/*
 * main.h
 *
 * Copyright 2014 Edward V. Emelianoff <eddy@sao.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
#pragma once
#ifndef __MAIN_H__
#define __MAIN_H__

extern unsigned long Global_time; // global time in ms

#define UART_BUF_LEN 8			// max 7 bytes transmited in on operation

#define EEPROM_KEY1	0xAE
#define EEPROM_KEY2	0x56

extern u8 UART_rx[];
extern u8 UART_rx_start_i;
extern u8 UART_rx_cur_i;

extern u8 US_flag;
extern float ratio;
extern volatile u16 ADC_value ;
extern volatile u16 Pulse_length;

//extern const U8 UART_devNUM;

void UART_send_byte(u8 byte);
void uart_write(char *str);
void printUint(u8 *val, u8 len);
void error_msg(char *msg);

#define check_UART_pointer(x) if(x == UART_BUF_LEN) x = 0;

#endif // __MAIN_H__
