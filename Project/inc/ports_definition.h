/*
 * ports_definition.h - definition of ports pins & so on
 *
 * Copyright 2014 Edward V. Emelianov <eddy@sao.ru, edward.emelianoff@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#pragma once
#ifndef __PORTS_DEFINITION_H__
#define __PORTS_DEFINITION_H__

#include "stm8s.h"

// macro for using in port constructions like PORT(LED_PORT, ODR) = xx
#define CONCAT(a,b)		a##->##b
#define PORT(a,b)		CONCAT(a,b)

// on-board LED
#ifndef KICARETV
#define LED_PORT			GPIOC
#define LED_PIN				GPIO_PIN_2
#else
#define LED_PORT			GPIOD
#define LED_PIN				GPIO_PIN_0
#endif

// UART2_TX
#define UART_TX_PORT	GPIOD
#define UART_TX_PIN		GPIO_PIN_5
// UART2_RX
#define UART_RX_PORT	GPIOD
#define UART_RX_PIN		GPIO_PIN_6

// Ultrasonic interface
#ifndef KICARETV
// out: PE5
#define US_OUT_PORT		GPIOE
#define US_OUT_PIN		GPIO_PIN_5
// pulse width measurement: PC1 - TIM1_CH1
#define US_IN_PORT		GPIOC
#define US_IN_PIN			GPIO_PIN_1

#else
// TRIG out: PB3
#define US_OUT_PORT		GPIOB
#define US_OUT_PIN		GPIO_PIN_3
// pulse width measurement: PD4 - TIM2_CH1
#define US_IN_PORT		GPIOD
#define US_IN_PIN			GPIO_PIN_4

#if 0
// Trim measurement: PF4 - AIN12
#define TRIM_IN_PORT	GPIOB
#define TRIM_IN_PIN		GPIO_PIN_0

// HOTPLUG SW: PC3
#define SW_OUT_PORT		GPIOE
#define SW_OUT_PIN		GPIO_PIN_5
#else
// Trim measurement: PF4 - AIN12
#define TRIM_IN_PORT	GPIOF
#define TRIM_IN_PIN		GPIO_PIN_4

// HOTPLUG SW: PC3
#define SW_OUT_PORT		GPIOC
#define SW_OUT_PIN		GPIO_PIN_3
#endif
// HOTPLUG SRC: PC2
#define HP_SRC_IN_PORT	GPIOC
#define HP_SRC_IN_PIN		GPIO_PIN_2

#ifdef USE_SPI
//SPI_NSS
#define SPI_NSS_PORT	GPIOE
#define SPI_NSS_PIN		GPIO_PIN_5
//SPI_CLK
#define SPI_CLK_PORT	GPIOC
#define SPI_CLK_PIN		GPIO_PIN_5
//SPI_MOSI
#define SPI_MOSI_PORT	GPIOC
#define SPI_MOSI_PIN	GPIO_PIN_6
//SPI_NSS
#define SPI_MISO_PORT	GPIOC
#define SPI_MISO_PIN	GPIO_PIN_7
#endif

#ifdef USE_I2C
//SPI_MOSI
#define I2C_SDA_PORT	GPIOB
#define I2C_SDA_PIN		GPIO_PIN_5
//SPI_NSS
#define I2C_SCL_PORT	GPIOB
#define I2C_SCL_PIN		GPIO_PIN_4
#endif

#ifdef EXT_PORT
#define EXT_A0_PORT		GPIOB
#define EXT_A0_PIN		GPIO_PIN_0
#define EXT_A1_PORT		GPIOB
#define EXT_A1_PIN		GPIO_PIN_1
#define EXT_A2_PORT		GPIOB
#define EXT_A2_PIN		GPIO_PIN_2
#define EXT_D0_PORT		GPIOD
#define EXT_D0_PIN		GPIO_PIN_2
#define EXT_D1_PORT		GPIOD
#define EXT_D1_PIN		GPIO_PIN_3
#define EXT_D2_PORT		GPIOD
#define EXT_D2_PIN		GPIO_PIN_7
#define EXT_D3_PORT		GPIOC
#define EXT_D3_PIN		GPIO_PIN_3
#endif

#endif

#endif // __PORTS_DEFINITION_H__
