/*
 * main.c
 *
 * Copyright 2014 Edward V. Emelianoff <eddy@sao.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
/**
  ******************************************************************************
  * @file main.c
  * @brief This file contains the main function for this template.
  * @author STMicroelectronics - MCD Application Team
  * @version V2.0.0
  * @date 15-March-2011
  ******************************************************************************
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  * @image html logo.bmp
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"
#include "ports_definition.h"
#include "stm8_tsl_api.h"
#include "main.h"
#include "float.h"

/* Private defines -----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
void US_start(void);
void US_check(void);
void delay(u16 value);
/*
 * 0 0000
 * 1 0001
 * 2 0010
 * 3 0011
 * 4 0100
 * 5 0101
 * 6 0110
 * 7 0111
 * 8 1000
 * 9 1001
 * a 1010
 * b 1011
 * c 1100
 * d 1101
 * e 1110
 * f 1111
 */
#pragma section [user_setting]
u8 SystemState;
u8 Switchon_Interval = 7;
u8 Switchoff_Interval = 10;
#pragma section []
u32 data_address;							// first data EEPROM byte address 

unsigned long Global_time = 0L; // global time in ms
u16 paused_val = 500; // interval between LED flashing
bool loop = 1;
volatile u16 red_zone = 100;
volatile u16 ADC_value = 0; // value of last ADC measurement

volatile u16 Pulse_length = 11; // length of ultrasonic echo pulse
u8 US_flag = 0;// 0 - conversion in progress; 1 - conversion error; 2 - conversion done; 3 - overflow
u8 Wait_flag = 0;
float ratio = 1;

u8 UART_rx[UART_BUF_LEN]; // cycle buffer for received data
u8 UART_rx_start_i = 0;   // started index of received data (from which reading starts)
u8 UART_rx_cur_i = 0;     // index of current first byte in rx array (to which data will be written)
// ATTENTION! to change global variable in PROGRAM memory, it should be CONST!!!
//const u8 UART_devNUM = THIS_DEVICE_NUM; // device number, master sais it

void delay(u16 value) {
	u8 i;
	for(i = 0; i < value; i++) nop();
}
/**
 * Send one byte through UART
 * @param byte - data to send
 */
void UART_send_byte(u8 byte){
	UART2->DR = byte;
	while(!(UART2->SR & UART2_SR_TC));
}

void uart_write(char *str){
	while(*str){
		UART2->DR = *str++;
		while(!(UART2->SR & UART2_SR_TC));
	}
}

/**
 * Read one byte from Rx buffer
 * @param byte - where to store readed data
 * @return 1 in case of non-empty buffer
 */
u8 UART_read_byte(u8 *byte){
	if(UART_rx_start_i == UART_rx_cur_i) // buffer is empty
		return 0;
	*byte = UART_rx[UART_rx_start_i++];
	check_UART_pointer(UART_rx_start_i);
	return 1;
}

void printUint(u8 *val, u8 len){
	unsigned long Number = 0;
	u8 i = len;
	char ch;
	u8 decimal_buff[12]; // max len of U32 == 10 + \n + \0
	if(len > 4 || len == 3 || len == 0) return;
	for(i = 0; i < 12; i++)
		decimal_buff[i] = 0;
	decimal_buff[10] = '\n';
	ch = 9;
	switch(len){
		case 1:
			Number = *((u8*)val);
			break;
		case 2:
			Number = *((u16*)val);
		break;
		case 4:
			Number = *((unsigned long*)val);
		break;
	}
	do{
		i = Number % 10L;
		decimal_buff[ch--] = i + '0';
		Number /= 10L;
	}while(Number && ch > -1);
	uart_write((char*)&decimal_buff[ch+1]);
}
/*
u8 u8toHEX(u8 val){
	val &= 0x0f;
	if(val < 10) val += '0';
	else val += 'a' - 10;
	return val;
}

void printUintHEX(u8 *val, u8 len){
	u8 i, V;
	uart_write("0x");
	for(i = 0; i < len; i++){
		V = *val++;
		UART_send_byte(u8toHEX(V>>4)); // MSB
		UART_send_byte(u8toHEX(V));    // LSB
	}
	UART_send_byte('\n');
}*/

u8 readInt(int *val){
	unsigned long T = Global_time;
	unsigned long R = 0;
	int readed;
	u8 sign = 0, rb, ret = 0, bad = 0;
	do{
		if(!UART_read_byte(&rb)) continue;
		if(rb == '-' && R == 0){ // negative number
			sign = 1;
			continue;
		}
		if(rb < '0' || rb > '9') break; // number ends with any non-digit symbol that will be omitted
		ret = 1; // there's at least one digit
		R = R * 10L + rb - '0';
		if(R > 0x7fff){ // bad 			R = 0;
			bad = 0;
		}
	}while(Global_time - T < 10000); // wait no longer than 10s
	if(bad || !ret) return 0;
	readed = (int) R;
	if(sign) readed *= -1;
	*val = readed;
	return 1;
}

void error_msg(char *msg){
	uart_write("\nERROR: ");
	uart_write(msg);
	UART_send_byte('\n');
}

/**
 * Change variable stored in program memory
 * !!! You can change only const values (non-constants are initializes on program start)
 * @param addr - variable address
 * @param new value
 * @return 0 in case of error
 */
u8 change_progmem_value(u8 *addr, u8 val){
	// unlock memory
	FLASH->PUKR = EEPROM_KEY2;
	FLASH->PUKR = EEPROM_KEY1;
	// check bit PUL=1 in FLASH_IAPSR
	if(!FLASH->IAPSR & 0x02)
		return 0;
	*addr = val;
	// clear PUL to lock write
	FLASH->IAPSR &= ~0x02;
	return 1;
}

/**
  * @brief Data Flash unlocking routine
  * @par Parameters: None
  * @retval None
  */
void UnlockDataFlash(void) 
{
	volatile u8 DataUnlocked = 0;
	
	/* Unlock Data memory Keys registers */		
	while( DataUnlocked == 0 )
	{
		FLASH->DUKR = EEPROM_KEY1;
		FLASH->DUKR = EEPROM_KEY2;
		
		DataUnlocked = (u8)FLASH->IAPSR;	
		DataUnlocked &= FLASH_IAPSR_DUL;
	}
}

/**
  * @brief Byte Flash writing routine
  * @par Parameters:
  * add: destination adress in flash
	* data: byte to be written in flash
  * @retval None
  */
void WriteFlash_Byte(u32 add, u8 byte) 
{	
	FLASH->CR1 &= (u8)(~FLASH_CR1_FIX);		// Set Standard programming time (max 6.6 ms) 
	
	*((PointerAttr u8*) add) = byte;
}

/**
  * @brief Byte Flash reading routine
  * @par Parameters:
  * add: destination adress in flash
  * @retval Return value:
	* data: byte read in flash at address add
  */
u8 ReadFlash_Byte(u32 add) 
{
	return(*((u8*) add));
}

/**
  * @brief Reset handling routine
  * @par Parameters: None
  * @retval: None
  */
void ResetHandling(u16 time_out) 
{	
	// 								*** TESTING RESET CONDITIONS ***
	if((RST->SR & RST_SR_WWDGF) != 0) // Reset by WWDOG?
	{	
		RST->SR = RST_SR_WWDGF;					// YES - clear WWDG reset status
	} 
	else  if((RST->SR & RST_SR_IWDGF) != 0) // Reset by IWDOG? 
	{ 
		RST->SR = RST_SR_IWDGF;		// YES - clear IWDG status
		SystemState = 0;		// re-entering first state
	} 
	else 
	{
		SystemState = 0;
	}
}
/*
u8 change_eeprom_value(u8 *addr, u8 val){
	// unlock memory
	FLASH_DUKR = EEPROM_KEY1;
	FLASH_DUKR = EEPROM_KEY2;
	// check bit DUL=1 in FLASH_IAPSR
	if(!FLASH_IAPSR & 0x08)
		return 0;
	*addr = val;
	// clear DUL to lock write
	FLASH_IAPSR &= ~0x08;
	return 1;
}
*/

/**
 * Start measurements by ultrasonic distance meter
 * @param
 * @return
 */
void US_start(){
	
	TIM2_SetCounter(0);
	TIM2_SetCompare1(0);
	TIM2_SetCompare2(0);
	US_flag = 0;
	
	//PORT(US_OUT_PORT, ODR) &= ~US_OUT_PIN;	
	// post initial pulse
	GPIO_WriteHigh(US_OUT_PORT, US_OUT_PIN);
	//GPIO_WriteLOW(US_OUT_PORT, US_OUT_PIN);
	//PORT(US_OUT_PORT, ODR) |= US_OUT_PIN;
	delay(20); // wait at least for 10us
	GPIO_WriteLow(US_OUT_PORT, US_OUT_PIN);
	//GPIO_WriteHigh(US_OUT_PORT, US_OUT_PIN);
	
		// Enable the captures by writing the CC1E and CC2E bits to 1 in the TIM1_CCER1 register.
	TIM2_CCxCmd(TIM2_CHANNEL_1, ENABLE);
	TIM2_CCxCmd(TIM2_CHANNEL_2, ENABLE);
	TIM2_Cmd(ENABLE);
}

/**
 * Check ultrasonic flag value and send message
 * @param
 * @return
 */
void US_check(){
	unsigned long L;
	if(!US_flag) 
		return;
	if(US_flag == 1){ // error - write message
		error_msg("measurement overcapture");
	}else if(US_flag == 3){ // overflow
		error_msg("TIM2 overflow");
	}else{ // all OK - write measured length
		Pulse_length = TIM2_GetCounter();
		uart_write("\nCount : ");
		printUint((u8*)&Pulse_length, 2);
		
		Pulse_length = TIM2_GetCapture1();
		uart_write("\nStart : ");
		printUint((u8*)&Pulse_length, 2);
		
		Pulse_length = TIM2_GetCapture2();
		uart_write("\nEnd : ");
		printUint((u8*)&Pulse_length, 2);
		// sound velocity == 340m/s in normal conditions
		// So distance = pulse * 1e-6(us) * 340 / 2
		// in millimeters this is equal of pulse*170/1000
		L = ((unsigned long) Pulse_length) * 44.56L;
		L /= 100L;
		uart_write("\nDistance (cm): ");
		printUint((u8*)&L, 4);
		ratio = ADC_value / 512.0f;
		#if 1
		red_zone = 100 * ratio;
		#endif
		uart_write("\nThreshold : ");
		printUint((u8*)&(red_zone), 2);
		if(Wait_flag == 0) { 
			if(L < red_zone) {
				GPIO_WriteHigh(SW_OUT_PORT, SW_OUT_PIN);
				Wait_flag = Switchoff_Interval;
			} else {
				GPIO_WriteLow(SW_OUT_PORT, SW_OUT_PIN);
				Wait_flag = Switchon_Interval;
			}
		} else {
			Wait_flag--;
		}
	}
	US_flag = 0;
}

int main(){
	unsigned long T = 0L;
	//int Ival;
	unsigned long ul;
	u16 u;
	u8 rb;
	ErrorStatus ret;
	data_address = 0x4000;	// data EEPROM first byte address
	
	UnlockDataFlash();			// unlock data flash for further writing
	
	CFG->GCR |= 1; // disable SWIM

	// Configure clocking
	CLK_SYSCLKConfig(CLK_PRESCALER_CPUDIV1);
	ret = CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO, CLK_SOURCE_HSE, ENABLE, CLK_CURRENTCLOCKSTATE_ENABLE);
	if( ret == ERROR) {
		CLK_ClockSwitchCmd(DISABLE);
		CLK_SYSCLKEmergencyClear();
		CLK_HSECmd(DISABLE);
		CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
		ret = CLK_ClockSwitchConfig(CLK_SWITCHMODE_AUTO, CLK_SOURCE_HSI, ENABLE, CLK_CURRENTCLOCKSTATE_ENABLE);
	} else
		CLK_HSECmd(ENABLE);
	//CLK_HSECmd(ENABLE);
	//CLK->CKDIVR = 0; // F_HSI = 16MHz, f_CPU = 16MHz

	// Timer 4 (8 bit) used as system tick timer
	// prescaler == 128 (2^7), Tfreq = 125kHz
	// period = 1ms, so ARR = 125
	CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER4, ENABLE);
	TIM4_TimeBaseInit(TIM4_PRESCALER_128, 125);
	// interrupts: update
	TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
	//TIM4->IER = TIM4_IER_UIE;
	// auto-reload + interrupt on overflow + enable
	TIM4_UpdateRequestConfig(TIM4_UPDATESOURCE_GLOBAL);
	TIM4_ARRPreloadConfig(ENABLE);
	TIM4_Cmd(ENABLE);
	
	//CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER3, ENABLE);
	//TIM3_TimeBaseInit(TIM3_PRESCALER_128, 125);
	//TIM3_ITConfig(TIM3_IT_UPDATE, ENABLE);

	GPIO_Init(US_IN_PORT, US_IN_PIN, GPIO_MODE_IN_PU_NO_IT);
	GPIO_Init(US_OUT_PORT, US_OUT_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
	//GPIO_ExternalPullUpConfig(US_OUT_PORT, US_OUT_PIN, DISABLE);
	// Configure Timer2 for measurement of US pulse width:
	// main frequency: 1MHz
	// prescaler = f_{in}/f_{tim2} - 1
	// set Timer2 to 1MHz: 16/1 - 1 = 15
	CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER2, ENABLE);
	TIM2_TimeBaseInit(TIM2_PRESCALER_512, 31250);
	// capture/compare channel
	// channel CC1 (0->1) stores low pulse length,
	// channel CC2 (1->0) stores time period between two consequent zero-transitions
	TIM2_ITConfig(TIM2_IER_CC2IE | TIM2_IER_CC1IE | TIM2_IER_UIE, ENABLE);// enable interrupt on CC2 & overflow

	// 1. Select the active input capture or trigger input for TIM2_CCR1 by writing the CC1S bits
	// 		to 01 in the TIM1_CCMR1 register (TI1FP1 selected).
	// IC1F = 0 - no filter
	// IC1PSC = 0 - no prescaler
	// TIM2_CCMR1: IC1F[3:0] | IC1PSC[1:0] | CC1S[1:0]
	TIM2_ICInit(TIM2_CHANNEL_1, TIM2_ICPOLARITY_RISING, TIM2_ICSELECTION_DIRECTTI,
		TIM2_ICPSC_DIV1, 0);
	//TIM2->CCMR1 = 1;
	
	// 3. Select the active input for TIM2_CCR2 by writing the CC2S bits to 10 in the
	//		TIM2_CCMR2 register (TI1FP2 selected).
		// 2. Select the active polarity for TI1FP1 (used for both capture and counter clear in
	// 		TIMx_CCR1) by writing the CC1P bit to 0 (TI1FP1 active on rising edge).
	// 4. Select the active polarity for TI1FP2 (used for capture in TIM2_CCR2) by writing the
	//		CC2P bit to 1 (TI1FP2 active on falling edge).
	// TIM2_CCER1: CC2NP | CC2NE | CC2P | CC2E | CC1NP | CC1NE | CC1P | CC1E
	TIM2_ICInit(TIM2_CHANNEL_2, TIM2_ICPOLARITY_FALLING, TIM2_ICSELECTION_INDIRECTTI,
		TIM2_ICPSC_DIV1, 0);
	//TIM2_OC2Init(TIM2_OCMODE_INACTIVE, TIM2_OUTPUTSTATE_DISABLE, 0, TIM2_OCPOLARITY_LOW);
	//TIM2->CCMR2 = 2;
	//TIM2->CCER1 = 0x20;

	// Configure ADC
#ifdef KICARETV
	GPIO_Init(TRIM_IN_PORT, TRIM_IN_PIN, GPIO_MODE_IN_FL_NO_IT);
	CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER1, ENABLE);
	TIM1_TimeBaseInit(15, TIM1_COUNTERMODE_UP, 0xFFFF, 0);
	TIM1_Cmd(ENABLE);
	CLK_PeripheralClockConfig(CLK_PERIPHERAL_ADC, ENABLE);
	ADC1_Cmd(DISABLE);
	// select PF4[AIN12] & enable interrupt for EOC
	//ADC1->CSR = 0x2C;
#if 0
	ADC1_Init(ADC1_CONVERSIONMODE_CONTINUOUS, ADC1_CHANNEL_0, ADC1_PRESSEL_FCPU_D18,
		ADC1_EXTTRIG_TIM, DISABLE, ADC1_ALIGN_RIGHT, ADC1_SCHMITTTRIG_CHANNEL0, DISABLE);
#else
	ADC1_Init(ADC1_CONVERSIONMODE_CONTINUOUS, ADC1_CHANNEL_12, ADC1_PRESSEL_FCPU_D18,
		ADC1_EXTTRIG_TIM, DISABLE, ADC1_ALIGN_RIGHT, ADC1_SCHMITTTRIG_CHANNEL12, DISABLE);
#endif
	ADC1_ITConfig(ADC1_IT_EOCIE, ENABLE);
	//ADC1->TDRH = 0x10; // disable Schmitt triger for AIN12
	// right alignment
	//ADC1->CR2 = 0x0A; // scan mode: first read ADC_DRL!
	
	// ADC1->CR3 =0x80; //Data buffer conversion
	// f_{ADC} = f/18 & continuous non-buffered conversion & wake it up
	//ADC1->CR1 = 0x72; 
	ADC1_Cmd(ENABLE); // turn on ADC (this needs second write operation)
#else
	// select PD2[AIN3] & enable interrupt for EOC
	ADC1->CSR = 0x23;
	ADC1->TDRL = 0x08; // disable Schmitt triger for AIN3
	// right alignment
	ADC1->CR2 = 0x08; // don't forget: first read ADC_DRL!
	// f_{ADC} = f/18 & continuous non-buffered conversion & wake it up
	ADC1->CR1 = 0x73;
	ADC1->CR1 = 0x73; // turn on ADC (this needs second write operation)
	
#endif
	
	// Configure pins
	// PC2 - PP output (on-board LED)
	GPIO_Init(LED_PORT, LED_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
	//GPIO_ExternalPullUpConfig(LED_PORT, LED_PIN, ENABLE);
	GPIO_Init(SW_OUT_PORT, SW_OUT_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
	//GPIO_ExternalPullUpConfig(SW_OUT_PORT, SW_OUT_PIN, DISABLE);
	
	// PD5 - UART2_TX -- pseudo open-drain output; don't forget an pullup resistor!
	GPIO_Init(UART_TX_PORT, UART_TX_PIN, GPIO_MODE_OUT_PP_HIGH_FAST);
	GPIO_ExternalPullUpConfig(UART_TX_PORT, UART_TX_PIN, DISABLE);
	GPIO_Init(UART_RX_PORT, UART_RX_PIN, GPIO_MODE_IN_PU_NO_IT);
	GPIO_ExternalPullUpConfig(UART_RX_PORT, UART_RX_PIN, DISABLE);
	// Configure UART (UART_CR3 = 0 - reset value)
	// 57600 on 16MHz: BRR1=0x11, BRR2=0x06
	CLK_PeripheralClockConfig(CLK_PERIPHERAL_UART2, ENABLE);
	UART2_Init(57600, UART2_WORDLENGTH_8D, UART2_STOPBITS_1, UART2_PARITY_NO, 
		UART2_SYNCMODE_CLOCK_DISABLE, UART2_MODE_TXRX_ENABLE);
	UART2_ITConfig(UART2_IT_RXNE_OR, ENABLE);
	UART2_Cmd(ENABLE);
	//UART2->BRR1 = 0x11; 
	//UART2->BRR2 = 0x06;
//#ifndef KICARETV
//	UART2->CR1 = UART2_CR1_M; // M = 1 -- 9bits
//#else
//	UART2->CR1 = UART2_CR1_R8 | UART2_CR1_T8; // Tx Rx 8 bits
//	UART2->CR3 &= ~0x30;	//Stop 1 bit
//#endif

	//UART2->CR2 = UART2_CR2_TEN | UART2_CR2_REN | UART2_CR2_RIEN; // Allow RX, generate ints on rx

	// enable all interrupts
	enableInterrupts();
	
	// 								*** IWDOG INITIALIZATION (LSI 128khz/2)***		
	IWDG_Enable();	// enable and start the iwdog counter at first
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);		// unlock iwdog configuration registers
	IWDG_SetPrescaler(IWDG_Prescaler_256);		// divider /256
	IWDG_SetReload(0xFF);		// max refresh period  1.02 s
	IWDG_ReloadCounter();		// lock iwdog registers & reload the iwdog counter
	
	/* Infinite loop */
	while (1)
	{
		if((Global_time - T > paused_val) || (T > Global_time)){
			T = Global_time;
			if(!(BitStatus)(GPIO_ReadOutputData(SW_OUT_PORT) & SW_OUT_PIN))
				GPIO_WriteReverse(LED_PORT, LED_PIN);
			else
				GPIO_WriteHigh(LED_PORT, LED_PIN);
			if(loop)
				US_start();
			//PORT(LED_PORT, ODR) ^= LED_PIN; // blink on-board LED
			IWDG_ReloadCounter();			// iwdog refresh 
		}
		if(US_flag) US_check(); // end of measurement with ultrasonic?
		if(UART_read_byte(&rb)){ // buffer isn't empty
			switch(rb){
				case 'h': // help
				case 'H':
					uart_write("\nPROTO:\n"
						"+/-\tLED period\n"
						"A\tprint ADC value\n"
						"D\tmeasure distance by US\n"
						);
				break;
				case '+':
					paused_val += 100;
					if(paused_val > 10000)
						paused_val = 500; // but not more than 10s
				break;
				case '-':
					paused_val -= 100;
					if(paused_val < 100)  // but not less than 0.1s
						paused_val = 500;
				break;
				case 'A':
					ul = ADC_value * 3300L;
					u = (u16)(ul >> 10); // 0..3300 - U in mv
					printUint((u8*)&u, 2);
				break;
				case 'D':
					loop = 0;
					US_start();
				break;
				case 'R':
					loop = 1;
				break;
				case 'S':
					loop = 0;
				break;
			case '}':
					Switchon_Interval += 1;
					if(Switchon_Interval > 10)
						Switchon_Interval = 10; // but not more than 10s
				break;
				case '{':
					Switchon_Interval -= 1;
					if(Switchon_Interval < 1)  // but not less than 0.1s
						Switchon_Interval = 1;
				break;
				case ']':
					Switchoff_Interval += 1;
					if(Switchoff_Interval > 20)
						Switchoff_Interval = 20; // but not more than 10s
				break;
				case '[':
					Switchoff_Interval -= 1;
					if(Switchoff_Interval < 1)  // but not less than 0.1s
						Switchoff_Interval = 1;
				break;
/*				case 'N':
					if(readInt(&Ival) && Ival > 0 && Ival < 256)
						if(!change_progmem_value(&UART_devNUM, (unsigned int) Ival))
							error_msg("can't change val");
				break;
*/
				default:
				break;
			}
		}
	}
}

#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval : None
  */
void assert_failed(u8* file, u32 line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
