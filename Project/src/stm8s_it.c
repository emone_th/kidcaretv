/*
 * interrupts.c
 *
 * Copyright 2014 Edward V. Emelianoff <eddy@sao.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
 /**
  ******************************************************************************
  * @file stm8s_it.c
  * @author STMicroelectronics - MCD Application Team
  * @version V2.0.0
  * @date 15-March-2011
  * @brief Main Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8s_it.h"
#include "ports_definition.h"
#include "main.h"

/** @addtogroup I2C_EEPROM
  * @{
  */
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile u8 in_measure; // flag of measurenent

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/* Public functions ----------------------------------------------------------*/
#ifdef _COSMIC_
/**
  * @brief  Dummy interrupt routine
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(NonHandledInterrupt, 25)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /*_COSMIC_*/

/**
  * @brief  TRAP interrupt routine
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}
/**
  * @brief  Top Level Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TLI_IRQHandler, 0)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

/**
  * @brief  Auto Wake Up Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(AWU_IRQHandler, 1)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

/**
  * @brief  Clock Controller Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(CLK_IRQHandler, 2)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
	if(CLK_GetITStatus(CLK_IT_SWIF)) {
		CLK_ClearITPendingBit(CLK_IT_SWIF);
	}
}

/**
  * @brief  External Interrupt PORTA Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI_PORTA_IRQHandler, 3)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

/**
  * @brief  External Interrupt PORTB Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI_PORTB_IRQHandler, 4)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

/**
  * @brief  External Interrupt PORTC Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI_PORTC_IRQHandler, 5)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

/**
  * @brief  External Interrupt PORTD Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI_PORTD_IRQHandler, 6)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

/**
  * @brief  External Interrupt PORTE Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}
#ifdef STM8S903
/**
  * @brief  External Interrupt PORTF Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(EXTI_PORTF_IRQHandler, 8)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /*STM8S903*/

#ifdef STM8S208
/**
  * @brief  CAN RX Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(CAN_RX_IRQHandler, 8)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

/**
  * @brief  CAN TX Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(CAN_TX_IRQHandler, 9)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /*STM8S208 || STM8AF52Ax */

/**
  * @brief  SPI Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(SPI_IRQHandler, 10)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}


/**
  * @brief  Timer1 Update/Overflow/Trigger/Break Interrupt routine.
  * @param  None
  * @retval None
	
	// Timer1 Update/Overflow/Trigger/Break Interrupt
	// in_measure == 1 means that we will get wrong result as pulse length is too long
	// pulse of 65536us is 32768us to something == near 11 meters
  */
INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_BRK_IRQHandler, 11)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
	if(TIM1_GetITStatus(TIM1_IT_UPDATE)) {
		//TIM2->SR1 &= ~TIM2_SR1_UIF; // clear this flag
		TIM1_ClearITPendingBit(TIM1_IT_UPDATE);
	}
}

/**
  * @brief  Timer1 Capture/Compare Interrupt routine.
  * @param  None
  * @retval None
	// Timer1 Capture/Compare Interrupt routine.
	// pulse width measurement: PC1 - TIM1_CH1
  */
INTERRUPT_HANDLER(TIM1_CAP_COM_IRQHandler, 12)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
		/*U16 P = TIM1_CNTRH;
	P |= TIM1_CNTRL << 8;
	printUint((u8*)&P,2);
	printUint(&TIM1_CCR2H,1);
	printUint(&TIM1_CCR2L,1);
	printUint(&TIM1_CCR1H,1);
	printUint(&TIM1_CCR1L,1);
	printUint(&TIM1_CNTRH,1);
	printUint(&TIM1_CNTRL,1);*/
	//Pulse_length =  ((U16)TIM1_CCR2H << 8) | TIM1_CCR2L;
	
	/*u8 value = TIM1->SR1;
	if(value & TIM1_SR1_CC2IF){
		TIM1->SR1 = 0; // clear all interrupt flags
		TIM1->CR1 = 0;
		TIM1->CCER1 &= ~0x11; // stop timer
		in_measure = 0; // work done
	//	uart_write("CCR2\n");
		if(TIM1->SR2){ // overcapture: noice etc.
			TIM1->SR2 = 0;
			US_flag = 1;
		}else{
			US_flag = 2;
		}
	}*/
	//if(value & TIM1_SR1_CC1IF){
	//	TIM1->SR1 &= ~TIM1_SR1_CC1IF; // clear this interrupt flags
	//	in_measure = 1; // start of measurement
	//	uart_write("CCR1\n");
	/*	TIM1->CNTRH = 0; TIM1->CNTRL = 0;
		TIM1->CCR2H = 0; TIM1->CCR2L = 0;
		TIM1->CCR1H = 0; TIM1->CCR1L = 0;*/
	//}
}

#ifdef STM8S903
/**
  * @brief  Timer5 Update/Overflow/Break/Trigger Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(TIM5_UPD_OVF_BRK_TRG_IRQHandler, 13)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}
/**
  * @brief  Timer5 Capture/Compare Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(TIM5_CAP_COM_IRQHandler, 14)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

#else /*STM8S208, STM8S207, STM8S105 or STM8S103 or STM8AF62Ax or STM8AF52Ax or STM8AF626x */
/**
  * @brief  Timer2 Update/Overflow/Break Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(TIM2_UPD_OVF_BRK_IRQHandler, 13)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
	//u8 value = TIM2->SR1;

	if(TIM2_GetITStatus(TIM2_IT_UPDATE)) {
		//TIM2->SR1 &= ~TIM2_SR1_UIF; // clear this flag
		TIM2_ClearITPendingBit(TIM2_IT_UPDATE);
		if(!in_measure) return; // not process when there's nothing started
		//uart_write("OVF\n");
		if (TIM2_GetFlagStatus(TIM2_FLAG_CC1OF) || TIM2_GetFlagStatus(TIM2_FLAG_CC2OF)) {
			US_flag = 3;
			//}
			//TIM2->SR1 = 0;
			//TIM2->SR2 = 0;
			//TIM2->CR1 = 0;
			TIM2_Cmd(DISABLE);
			//TIM2->CCER1 &= ~0x11; // stop timer
			TIM2_CCxCmd(TIM2_CHANNEL_1, DISABLE);
			TIM2_CCxCmd(TIM2_CHANNEL_2, DISABLE);
			in_measure = 0;
		}
	}
}

/**
  * @brief  Timer2 Capture/Compare Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(TIM2_CAP_COM_IRQHandler, 14)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
	if(TIM2_GetITStatus(TIM2_IT_CC2)){		
		TIM2_ClearITPendingBit(TIM2_IT_CC2);
		//TIM2->SR1 = 0; // clear all interrupt flags
		//TIM2->CR1 = 0;
		TIM2_Cmd(DISABLE);
		//TIM2->CCER1 &= ~0x11; // stop timer
		TIM2_CCxCmd(TIM2_CHANNEL_1, DISABLE);
		TIM2_CCxCmd(TIM2_CHANNEL_2, DISABLE);
		in_measure = 0; // work done
	//	uart_write("CCR2\n");
		if(TIM2->SR2){ // overcapture: noice etc.
			TIM2->SR2 = 0;
			US_flag = 1;
		}else{
			US_flag = 2;
		}
	}
	if(TIM2_GetITStatus(TIM2_IT_CC1)){
		//TIM1->SR1 &= ~TIM1_SR1_CC1IF; // clear this interrupt flags
		TIM2_ClearITPendingBit(TIM2_IT_CC1);
		in_measure = 1; // start of measurement
	//	uart_write("CCR1\n");
	/*	TIM1->CNTRH = 0; TIM1->CNTRL = 0;
		TIM1->CCR2H = 0; TIM1->CCR2L = 0;
		TIM1->CCR1H = 0; TIM1->CCR1L = 0;*/
	}
}
#endif /*STM8S903*/

#if defined (STM8S208) || defined(STM8S207) || defined(STM8S105)
/**
  * @brief  Timer3 Update/Overflow/Break Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(TIM3_UPD_OVF_BRK_IRQHandler, 15)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
	if(TIM3_GetITStatus(TIM3_IT_UPDATE)){
		TIM3_ClearITPendingBit(TIM3_IT_UPDATE);
		//TODO: Reset wait flag
		//TIM3_Cmd(DISABLE);
		//Wait_flag = 0;
	}
}

/**
  * @brief  Timer3 Capture/Compare Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(TIM3_CAP_COM_IRQHandler, 16)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /*STM8S208, STM8S207 or STM8S105 or STM8AF62Ax or STM8AF52Ax or STM8AF626x */

#if defined (STM8S208) || defined(STM8S207) || defined(STM8S103) || defined (STM8AF62Ax) ||\
    defined (STM8AF52Ax) || defined (STM8S903)
/**
  * @brief  UART1 TX Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(UART1_TX_IRQHandler, 17)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

/**
  * @brief  UART1 RX Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(UART1_RX_IRQHandler, 18)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}
#endif /*STM8S105*/

/**
  * @brief  I2C Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(I2C_IRQHandler, 19)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

#if defined (STM8S105) || defined (STM8AF626x)
/**
  * @brief  UART2 TX interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(UART2_TX_IRQHandler, 20)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
  }

/**
  * @brief  UART2 RX interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(UART2_RX_IRQHandler, 21)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
		u8 rb;
		if(UART2->SR & UART2_SR_RXNE){ // data received
			rb = UART2->DR; // read received byte & clear RXNE flag
			UART_send_byte(rb); // echo received symbol
			UART_rx[UART_rx_cur_i++] = rb; // put received byte into cycled buffer
			if(UART_rx_cur_i == UART_rx_start_i){ // Oops: buffer overflow! Just forget old data
				UART_rx_start_i++;
				check_UART_pointer(UART_rx_start_i);
			}
			check_UART_pointer(UART_rx_cur_i);
		}
  }
#endif /* STM8S105*/

#if defined(STM8S207) || defined(STM8S208) || defined (STM8AF52Ax) || defined (STM8AF62Ax)
/**
  * @brief  UART3 TX interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(UART3_TX_IRQHandler, 20)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
  }

/**
  * @brief  UART3 RX interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(UART3_RX_IRQHandler, 21)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
  }
#endif /*STM8S208 or STM8S207 or STM8AF52Ax or STM8AF62Ax */

#if defined(STM8S207) || defined(STM8S208) || defined (STM8AF52Ax) || defined (STM8AF62Ax)
/**
  * @brief  ADC2 interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(ADC2_IRQHandler, 22)
{

    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
	
}
#else /*STM8S105, STM8S103 or STM8S903 or STM8AF626x */
/**
  * @brief  ADC1 interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(ADC1_IRQHandler, 22)
{

    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
	if(ADC1_GetITStatus(ADC1_IT_EOC))
	{
		ADC1_ClearITPendingBit(ADC1_IT_EOC);
#ifndef KICARETV
		ADC_value = ADC1->DRL; // in right-alignment mode we should first read LSB
		ADC_value |= ADC1->DRH << 8;
		ADC1->CSR &= 0x3f; // clear EOC & AWD flags
#else
		ADC_value = ADC1_GetConversionValue();
		//ADC1_ClearFlag(ADC1_FLAG_AWS12);
		//ADC1_ClearFlag(ADC1_FLAG_EOC);
		//ADC1->CSR &= 0x3f; // clear EOC & AWD flags
#endif
	}
}
#endif /*STM8S208 or STM8S207 or STM8AF52Ax or STM8AF62Ax */

#ifdef STM8S903
/**
  * @brief  Timer6 Update/Overflow/Trigger Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM6_UPD_OVF_TRG_IRQHandler, 23)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}
#else /*STM8S208, STM8S207, STM8S105 or STM8S103 or STM8AF62Ax or STM8AF52Ax or STM8AF626x */
/**
  * @brief  Timer4 Update/Overflow Interrupt routine.
  * @param  None
  * @retval None
  */
 INTERRUPT_HANDLER(TIM4_UPD_OVF_IRQHandler, 23)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
	if(TIM4->SR1 & TIM4_SR1_UIF){ // update interrupt
		Global_time++; // increase timer
	}
	TIM4->SR1 = 0; // clear all interrupt flags
}
#endif /*STM8S903*/

/**
  * @brief  Eeprom EEC Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EEPROM_EEC_IRQHandler, 24)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

/**
  * @}
  */

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
